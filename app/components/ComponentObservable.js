(function ($, App) {

    /**
     * Class ComponentObservable
     * @type {Object}
     */
    var ComponentObservable = App.Component.extend({
       constructor: function ComponentObservable(element, options) {
           App.Component.prototype.constructor.call(this, element, options);
           this.observers = [];
           addEventListeners.call(this);
       },
        addObserver: function (observer) {
            this.observers.push(observer);
        },
        removeObserver: function (observer) {
            var self = this;
            // $(this.observers).each(function (index, object) {
            //     if(object.__id == observer.__id){
            //         self.observers.splice(index, 1);
            //     }
            // })
        },
        notifyObservers: function (params) {
            $(this.observers).each(function (index, observer) {
                observer.__update(params);
            })
        }
    });

    function addEventListeners() {

    }

    App.ComponentObservable = ComponentObservable;

}(jQuery, PomodoroApp));