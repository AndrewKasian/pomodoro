(function ($, App, window) {

    var deferred;

    /**
     * Class-service LocalStorage
     * @type {Object}
     */
    var LocalStorage = App.Storage.extend({
        constructor: function (storageName, options) {
            this.super.constructor.call(this, storageName, options);
            this.storage = window.localStorage;
        },
        get: function (criteriaOrId) {
            deferred = $.Deferred();
            var results = [];
            if (isId(criteriaOrId)) {
                var itemById = findItemById.call(this, criteriaOrId);
                results.push(itemById);
                deferred.resolve(results);
            } else if(isCriteria(criteriaOrId)){
                results = findItemByCriteria.call(this, criteriaOrId);
                deferred.resolve(results);
            }
            return deferred.promise();
        },
        create: function (newItem) {
            deferred = $.Deferred();
            if(typeof newItem == "object"){
                newItem.__id = this.__generateId();
                var items = [];
                if(this.storage.getItem(this.storageName) !== null){
                    items = JSON.parse(this.storage.getItem(this.storageName));
                }
                items.push(newItem);
                items = JSON.stringify(items);
                this.storage.setItem(this.storageName, items);

                deferred.resolve(newItem);
            }else{
                deferred.reject('new item should be a object');
            }
            return deferred.promise();
        },
        update: function (itemId, item) {
            deferred = $.Deferred();

            var itemByID = findItemById.call(this, itemId);
            for(var newItemField in item){
                if(itemByID.hasOwnProperty(newItemField)){
                    itemByID[newItemField] = item[newItemField];
                }
            }
            deferred.resolve(itemByID);

            var items = JSON.parse(this.storage.getItem(this.storageName));

            $(items).each(function (index, itemCriterai){
                if(itemCriterai.__id == itemId){
                    items[index] = itemByID;
                }
            });

            items = JSON.stringify(items);
            this.storage.setItem(this.storageName, items);

            return deferred.promise();
        },

        delete: function (itemId) {
            deferred = $.Deferred();

            var items = JSON.parse(this.storage.getItem(this.storageName));
            $(items).each(function(index, itemCriteria){
                if(itemCriteria.__id == itemId){
                    items.splice(index, 1)
                }
            });
            deferred.resolve(items);

            items = JSON.stringify(items);
            this.storage.setItem(this.storageName, items);

            return deferred.promise();
        }
    });

    function isId(itemId) {
        if(typeof itemId == "string"){
            return true
        }
    }

    function isCriteria(criteria) {
        if(typeof criteria == "object"){
            return true
        }
    }

    function findItemById(id) {
        var $items = $(JSON.parse(this.storage.getItem(this.storageName)));
        var itemById = null;
        $items.each(function (index, item) {
            if(item.__id == id){
               itemById = item;
            }
        });
        return itemById;
    }

    function findItemByCriteria(criteria) {
        var $items = $(JSON.parse(this.storage.getItem(this.storageName)));
        var itemsByCriteria = [];
        $items.each(function (index, item) {
            var countCriteria = 0;
            for(var criteriaName in criteria){
                if(item.hasOwnProperty(criteriaName) && criteria[criteriaName] == item[criteriaName]){
                    countCriteria++;
                }
            }
            if(Object.keys(criteria).length == countCriteria){
                itemsByCriteria.push(item);
            }
        });
        return itemsByCriteria;
    }

    App.LocalStorage = LocalStorage;

}(jQuery, PomodoroApp, window));
