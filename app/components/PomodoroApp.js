(function ($, window) {

    /**
     * Main object for Pomodoro application
     * @type {{Object}}
     */
    var PomodoroApp = {};

    window.PomodoroApp = PomodoroApp;

}(jQuery, this));