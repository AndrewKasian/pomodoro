(function ($, App){

    /**
     * Class Clock
     * @type {Object}
     */
    var Clock = App.Component.extend({
        constructor: function (element, options) {
            App.Component.prototype.constructor.call(this, element, options);
            this.minutes = this.element.find('.minutes');
            this.seconds = this.element.find('.seconds');
            this.timer = null;
        },
        setTime: function(min, sec){
            min = ('0' + min).slice(-2);
            sec =  sec ? ('0' + sec).slice(-2) : '00';
            this.minutes.text(min);
            this.seconds.text(sec);
        },
        start: function() {
            var that = this;
            if(!this.timer) {
                this.timer = setInterval(function () {
                    runTime.call(that);
                }, 1000);
            }
        },
        pause: function() {
            var that = this;
            clearInterval(that.timer);
            this.timer = null;
        }

    });

    function runTime(){
        var min = this.minutes.text(),
            sec = this.seconds.text();

        if(min == 0 && sec == 0){
            this.pause();
            return false;
        }

        if(min > 0 && sec == 0){
            sec = 59;
            min = min - 1;
        }else if(sec > 0){
            sec = sec - 1;
        }

        this.setTime(min, sec);
    }

    App.Clock = Clock;

}(jQuery, PomodoroApp));