(function ($, App) {

    /**
     * Class TaskService
     * @type {Object}
     */
    var TaskService = App.Component.extend({
        constructor: function(element, options){
            this.super.constructor.call(this, element, options);
            this.tasks = PomodoroApp.StorageFactory.createStorage('tasks');
        },
        createTask: function(taskName) {
            var newTask = {
                taskName: taskName,
                pomidorka: 0,
                done: false,
                createAt: new Date()
            };
            return this.tasks.create(newTask);
        },
        getTaskById: function (id) {
            return this.getTasksByCriteria({__id: id})
        },
        getTodoTasks: function () {
            return this.getTasksByCriteria({done: false});
        },
        getFinishedTasks: function () {
            return this.getTasksByCriteria({done: true});
        },
        getTasksByCriteria: function(criteriaOrId) {
            return this.tasks.get(criteriaOrId);
        },
        editTask: function (id, newItemCriteria) {//newItemCriteria should be the Object
            var self = this;
            return this.tasks.update(id, newItemCriteria).then(function (item) {
                self.emit('task:update', {item: item});
                return item;
            });
        },
        deleteTask: function (id){
            return this.tasks.delete(id);
        }
    });

    var serviceInstance = null;
    App.TaskService = {
        getInstance: function () {
            if (!serviceInstance) {
                serviceInstance = new TaskService();
            }
            return serviceInstance;
        }
    };

}(jQuery, PomodoroApp));