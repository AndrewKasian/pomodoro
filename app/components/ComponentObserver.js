(function ($, App) {

    /**
     * Class ComponentObserver
     * @type {Object}
     */
    var ComponentObserver = App.Component.extend({
        constructor: function ComponentObserver(element, options) {
            App.Component.prototype.constructor.call(this, element, options);

        },
        __update : function (params) {

        }
    });

    App.ComponentObserver = ComponentObserver;

}(jQuery, PomodoroApp));