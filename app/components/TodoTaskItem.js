(function ($, App) {

    /**
     * TodoTaskItem default options
     * @type {{pomodoro: {mainBlockClass: string, countClass: string, imgClass: string}, controlButtonsList: {controlPanelClass: string, btnEditClass: string, btnDoneClass: string, btnDeleteClass: string}}}
     */
    var todoTaskItemDefault = {
        pomodoro: {
            mainBlockClass: 'item-pomodoro',
            countClass: 'pomodoro-count',
            imgClass: 'item-pomodoro-img'
        },
        controlButtonsList: {
            controlPanelClass: 'item-controls',
            btnEditClass: 'edit',
            btnDoneClass: 'done',
            btnDeleteClass: 'delete'
        },
        editInputFieldClass: 'editTaskField'
    };

    /**
     * Class TodoTaskItem
     * @type {Object}
     */
    var TodoTaskItem = App.TaskItem.extend({
        constructor: function TodoTaskItem(options) {
            this.options = App.TaskItem.prototype.mergeDefaultOptions.call(this, options);
            App.TaskItem.prototype.constructor.call(this, this.options);
            this.btnEdit = null;
            this.btnDone = null;
            this.btnDelete = null;

            this.__renderTaskItemElements();
            addEventListeners.call(this);
        },
        mergeDefaultOptions: function(options) {
            return $.extend({}, todoTaskItemDefault, options);
        },
        __renderTaskItemElements: function () {
            renderItemElements.call(this, this.options.itemParams);
            this.btnEdit = new App.Button(this.element.find('.' + this.options.controlButtonsList.btnEditClass));
            this.btnDone = new App.Button(this.element.find('.' + this.options.controlButtonsList.btnDoneClass));
            this.btnDelete = new App.Button(this.element.find('.' + this.options.controlButtonsList.btnDeleteClass));
        }

    });

    function renderItemElements (itemParams){

        var itemElements = createItemElements.call(this, itemParams);
        this.element.append(itemElements);
    }

    function createItemElements(dataItem) {
        var controlBtnOpt = this.options.controlButtonsList;
        var pomodoroOpt = this.options.pomodoro;

        var editInputField = '<input class="'+ this.options.editInputFieldClass +'" type="text" value="' + dataItem.taskName + '">';

        var itemControls = '<ul class="'+ controlBtnOpt.controlPanelClass +'">' +
                                '<li class="'+ controlBtnOpt.btnDoneClass +'"></li>' +
                                '<li class="'+ controlBtnOpt.btnDeleteClass +'"></li>' +
                                '<li class="'+ controlBtnOpt.btnEditClass +'"></li>' +
                            '</ul>';

        var itemPomidorka = '<div class="'+ pomodoroOpt.mainBlockClass +'">' +
                                '<span class="'+ pomodoroOpt.countClass +'">'+ dataItem.pomidorka +'</span>' +
                                '<img src="./assets/images/pomodoro.png" alt="" class="'+ pomodoroOpt.imgClass +'">' +
                            '</div>';

        return $(editInputField + itemControls + itemPomidorka);
    }

    function addEventListeners() {
        var self = this;

        this.btnDelete.on('button:click', function (){
            self.notifyObservers({ action: 'delete', item: self.options.itemParams });
        });

        this.btnDone.on('button:click', function (){
            self.notifyObservers({ action: 'done', item: self.options.itemParams });
        });

        this.btnEdit.on('button:click', function() {
            event.stopPropagation();
            self.notifyObservers({ action: 'update', item: self.element, itemOptions: self.options.itemParams });
        });

        this.element.on('click', function() {
            self.taskService.emit('item:choseActiveTask', self.options.itemParams.taskName);
            self.element.addClass('active').siblings().removeClass('active');
        })
    }

    App.TodoTaskItem = TodoTaskItem;


}(jQuery, PomodoroApp));
