(function ($, App) {

    /**
     * Class TaskList
     * @type {Object}
     */
    var TaskList = App.ComponentObserver.extend({
        constructor: function TaskList(element, options) {
            App.Component.prototype.constructor.call(this, element, options);
            this.taskService = PomodoroApp.TaskService.getInstance();
        }
    });


    App.TaskList = TaskList;

}(jQuery, PomodoroApp));