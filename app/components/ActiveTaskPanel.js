(function ($, App) {

    /**
     * Class ActiveTaskPanel
     * @type {Object}
     */
    var ActiveTaskPanel = App.Component.extend({
        constructor: function ActiveTaskItem(element, options){
            App.Component.prototype.constructor.call(this, element, options);
            this.taskService = PomodoroApp.TaskService.getInstance();
            addEventListeners.call(this);
        }
    });

    function addEventListeners() {
       var self = this;
       this.taskService.on('item:choseActiveTask', function (taskName) {
           self.element.text(taskName);
           self.element.hide();
       });
       this.taskService.on('timer:start', function() {
           self.element.show();
       })
    }

    App.ActiveTaskPanel = ActiveTaskPanel;

}(jQuery, PomodoroApp));