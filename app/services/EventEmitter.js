(function($, App) {

    /**
     * Service EventEmitter
     */
    var EventEmitter = Class.extend({
        constructor: function () {
            this.__handlers = {};
        },

        on : function(eventName, handler) {
            if (this.__handlers[eventName] === undefined) {
                this.__handlers[eventName] = {queue: []};
            }

            this.__handlers[eventName].queue.push(handler);
        },

        emit : function(eventName, data) {
            if (this.__handlers[eventName] === undefined) {
                return;
            }

            var items = this.__handlers[eventName].queue;
            items.forEach(function(item) {
                item(data || {});
            });
        }
    });

   App.EventEmitter = EventEmitter;

}(jQuery, PomodoroApp));