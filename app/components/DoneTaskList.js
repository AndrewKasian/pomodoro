(function ($, App) {

    /**
     * DoneTaslList default options
     * @type {{itemControls: {done: string}}}
     */
    var doneTaskListDefaultOptions = {
        itemControls: {
            done: '.done'
        }
    };

    /**
     * Class DoneTaskList
     * @type {Object}
     */
    var DoneTaskList = App.TaskList.extend({
        constructor: function DoneTaskList(element, options) {
            this.options = this.mergeDefaultOptions(options);
            App.TaskList.prototype.constructor.call(this, element, this.options);
            this.btnDone = null;
            this. __renderTaskList();
            addEventListeners.call(this);
        },
        mergeDefaultOptions: function(options) {
            return $.extend({}, doneTaskListDefaultOptions, options);
        },
        __renderTaskList: function(){
            this.taskService.getFinishedTasks().done(this.createTaskList.bind(this));
        },
        createTaskList: function(doneTaskList) {
            this.element.text('');
            var self = this;
            $(doneTaskList).each(function (index, itemParams) {
                var item = new App.DoneTaskItem({itemParams: itemParams});
                item.addObserver(self);
                self.element.append(item.getElement());
            });
        },
        __update: function (params) {
           if(params.action == 'done') {
               doneTaskItem.call(this, params.item);
            }else{
               console.warn('Wrong action: ', params.action);
           }
        }
    });

    function addEventListeners() {
        this.taskService.on('task:update', this.__renderTaskList.bind(this));
    }

    function doneTaskItem(item) {
        this.taskService.editTask(item.__id, {done: false});
    }

    App.DoneTaskList = DoneTaskList;

}(jQuery, PomodoroApp));