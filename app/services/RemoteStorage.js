(function ($, App) {

    /**
     * Class-service RemoteStorage
     * @type {Object}
     */
    var RemoteStorage = App.Storage.extend({
        constructor: function(options){
            this.super.constructor.call(this, options);
        }
    });

    App.RemoteStorage = RemoteStorage;

}(jQuery, PomodoroApp));