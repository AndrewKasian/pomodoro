(function ($, App) {

    /**
     * Storage default options
     * @type {{Object}}
     */
    var storageDefaultOptions = {

    };

    /**
     * Class Storage
     */
    var Storage = Class.extend({
        constructor: function(storageName, options) {
            this.storageName = storageName;
            this.options =  this.mergeDefaultOptions(options);
        },
        mergeDefaultOptions: function(options) {
            return $.extend({}, storageDefaultOptions, options);
        },
        __generateId: function(){
            return 'xxxx-xxxx-xxxx-yxxx-xxxxxxxx'.replace( /[xy]/g, function ( c ) {
                var r = Math.random() * 16 | 0;
                return ( c == 'x' ? r : ( r & 0x3 | 0x8 ) ).toString( 16 );
            } );
        },
        get: function (key, item) {

        },
        set: function (key, value) {

        },
        update: function (key, value) {

        },
        delete: function (key) {

        }
    });

    App.Storage = Storage;

}(jQuery, PomodoroApp));