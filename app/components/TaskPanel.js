(function ($, App) {

    /**
     * Class TaskPanel
     * @type {Object}
     */
    var TaskPanel = App.Component.extend({
       constructor: function(element, options){
           this.super.constructor.call(this, element, options);
           this.tabPanel = new App.TabPanel(this.options.tabPanel, {tabButtons: this.options.tabPanelBtn, tabContent: this.options.tabContent});
           this.todoTaskList = new App.TodoTaskList(this.options.todoTaskList);
           this.doneTaskList = new App.DoneTaskList(this.options.doneTaskList);
       }
    });


    App.TaskPanel = TaskPanel;

}(jQuery, PomodoroApp));