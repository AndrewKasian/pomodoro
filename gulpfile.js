var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss = require('gulp-minify-css');

//___________________
var conversion = {
    scss:['./assets/scss/main.scss'],
    allScss:['./assets/scss/*.scss']
};

gulp.task('all_css', function () {
    return gulp.src(conversion.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 4 versions'))
        .pipe(minifyCss())
        .pipe(gulp.dest('./assets/css'))
});

gulp.task('watcher',function(){
    gulp.watch(conversion.allScss, ['all_css']);
});

gulp.task('default', ['watcher']);

