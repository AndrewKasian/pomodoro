(function ($, App) {

    /**
     * DoneTaskItem default options
     * @type {{pomodoro: {mainBlockClass: string, countClass: string, imgClass: string}, controlButtonsList: {controlPanelClass: string, btnDoneClass: string}}}
     */
    var doneTaskItemDefault = {
        pomodoro: {
            mainBlockClass: 'item-pomodoro',
            countClass: 'pomodoro-count',
            imgClass: 'item-pomodoro-img'
        },
        controlButtonsList: {
            controlPanelClass: 'item-controls',
            btnDoneClass: 'done'
        }
    };

    /**
     * Class DoneTaskItem
     * @type {Object}
     */
    var DoneTaskItem = App.TaskItem.extend({
        constructor: function TodoTaskItem(options) {
            this.options = App.TaskItem.prototype.mergeDefaultOptions.call(this, options);
            App.TaskItem.prototype.constructor.call(this, this.options);
            this.btnDone = null;

            this.__renderTaskItemElements();
            addEventListeners.call(this);
        },
        mergeDefaultOptions: function(options) {
            return $.extend({}, doneTaskItemDefault, options);
        },
        __renderTaskItemElements: function () {
            renderItemElements.call(this, this.options.itemParams);
            this.btnDone = new App.Button(this.element.find('.' + this.options.controlButtonsList.btnDoneClass));
        }
    });

    function addEventListeners() {
        var self = this;
        this.btnDone.on('button:click', function (){
            self.notifyObservers({ action: 'done', item: self.options.itemParams });
        });
    }

    function renderItemElements (itemParams){
        var itemElements = createItemElements.call(this, itemParams);
        this.element.append(itemElements);
    }
    function createItemElements(dataItem) {
        var controlBtnOpt = this.options.controlButtonsList;
        var pomodoroOpt = this.options.pomodoro;
        var itemControls = '<ul class="'+ controlBtnOpt.controlPanelClass +'">' +
                                '<li class="'+ controlBtnOpt.btnDoneClass +'"></li>' +
                            '</ul>';
        var itemPomidorka = '<div class="'+ pomodoroOpt.mainBlockClass +'">' +
                                '<span class="'+ pomodoroOpt.countClass +'">'+ dataItem.pomidorka +'</span>' +
                                '<img src="./assets/images/pomodoro.png" alt="" class="'+ pomodoroOpt.imgClass +'">' +
                            '</div>';
        return $(itemControls + itemPomidorka);
    }

    App.DoneTaskItem = DoneTaskItem;

}(jQuery, PomodoroApp));