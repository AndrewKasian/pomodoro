(function ($, App) {

    /**
     * TodoTaskList default options
     * @type {{taskList: string, taskForm: string, formValue: string, formBtnCreate: string}}
     */
    var todoTaskListDefaultOptions = {
        taskListClass: 'task-list',
        taskFromWrapClass: 'new-task-form',
        taskFormValueClass: 'taskValue',
        taskFormBtnClass: 'create',
        itemControls: {
            done: '.done',
            delete: '.delete',
            edit: '.edit'
        }
    };
    /**
     * Class TodoTaskList
     * @type {Object}
     */
    var TodoTaskList = App.TaskList.extend({
        constructor: function TodoTaskList(element, options) {
            this.options = this.mergeDefaultOptions(options);
            App.TaskList.prototype.constructor.call(this, element, this.options);
            this.taskList = $('<ul class="'+ this.options.taskListClass +'"/>');
            this.taskForm = null;
            this.taskCreateBtn = null;
            this.taskFormValue = null;
            this.btnEdit = null;
            this.btnDelete = null;
            this.btnDone = null;
            this.__renderTaskList();
            addEventListeners.call(this);
        },
        mergeDefaultOptions: function(options) {
            return $.extend({}, todoTaskListDefaultOptions, options);
        },
        __renderTaskList: function(){
            this.element.append(this.taskList);
            createNewTaskForm.call(this);
            this.__renderTaskListItems();
        },
        __renderTaskListItems: function(){
            this.taskService.getTodoTasks().done(this.createTaskList.bind(this));
        },
        __update: function (params) {
            switch (params.action) {
                case 'delete':
                    deleteTaskItem.call(this, params.item);
                    break;
                case 'done':
                    doneTaskItem.call(this, params.item);
                    break;
                case 'update':
                    updateTaskItem.call(this, params.item, params.itemOptions);
                    break;
                default:
                    console.warn('Wrong action: ', params.action);
            }
        },
        createTaskList: function(todoTaskList) {
            this.taskList.text('');
            var self = this;
            $(todoTaskList).each(function (index, itemParams) {
                var item = new App.TodoTaskItem({itemParams: itemParams});
                item.addObserver(self);
                self.taskList.append(item.getElement());
            });
            this.btnEdit = new App.Button($(this.options.itemControls.edit));
            this.btnDelete = new App.Button($(this.options.itemControls.delete));
            this.btnDone = new App.Button($(this.options.itemControls.done));
        }
    });

    function createNewTaskForm (){
        var todoTaskForm =
            $('<div class="'+ this.options.taskFromWrapClass +'">' +
                '<ul>' +
                '<li class="'+ this.options.taskFormValueClass +'"><input type="text"></li>' +
                '<li class="'+ this.options.taskFormBtnClass +'">ADD TASK</li>' +
                '</ul>' +
                '</div>');
        this.element.append(todoTaskForm);
        newTaskFormInit.call(this)
    }

    function newTaskFormInit() {
        this.taskForm = $('.' + this.options.taskFromWrapClass);
        this.taskCreateBtn = new App.Button($('.' + this.options.taskFormBtnClass));
        this.taskFormValue = this.taskForm.find('input');
    }

    function addEventListeners() {
        var self = this;

        /**
         * Create New Task
         */
        this.taskCreateBtn.on('button:click', function(){
            var taskValue = self.taskFormValue.val();
            if(taskValue.length > 0){
                self.taskService.createTask(taskValue);
                self.__renderTaskListItems();
            }
            self.taskFormValue.val('');
        });

        /**
         * Check update tasks
         */
        this.taskService.on('task:update', this.__renderTaskListItems.bind(this));
    }

    function deleteTaskItem(item) {
        var self = this;
        this.taskService.deleteTask(item.__id).done(function () {
            self.__renderTaskListItems();
        })
    }

    function doneTaskItem(item) {
        this.taskService.editTask(item.__id, {done: true});
    }

    function updateTaskItem(item, itemOptions) {
        var inputField = item.find('input');
        showEditField(inputField);
        editTask.call(this, inputField, itemOptions);
    }

    function showEditField(inputField) {
        var focusAfterInputValue = inputField.attr('value');
        inputField.addClass('show').val(focusAfterInputValue).focus();
    }

    function editTask(inputField, itemOptions) {
        var self = this;
        inputField.on('keydown', function(event) {
            if(event.keyCode == 13){
                editInputValueChanges.call(self, inputField, itemOptions);
            }
        });
        inputField.one('focusout', function() {
            editInputValueChanges.call(self, inputField, itemOptions);
        });
    }

    function editInputValueChanges(inputField, itemOptions) {
        if(inputField.val().length > 0){
            saveEditTaskValue.call(this, inputField.val(), itemOptions);
        }else{
            inputField.removeClass('show');
        }
    }

    function saveEditTaskValue(taskNewValue, itemOptions) {
        var self = this;
        this.taskService.editTask(itemOptions.__id, {taskName: taskNewValue}).done(function (data) {
            self.__renderTaskListItems();
        })
    }

    App.TodoTaskList = TodoTaskList;

}(jQuery, PomodoroApp));