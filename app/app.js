$(function () {
    var timer = new PomodoroApp.Timer('.timer', {
        btnPomodoro: '.timer .work-time',
        btnShortBreak: '.timer .short-break',
        btnLongBreak: '.timer .long-break',
        btnTimerStart: '.timer .timer-start',
        btnTimerPause: '.timer .timer-pause',
        clock: '.timer .clock'
    });

    var taskPanel = new PomodoroApp.TaskPanel('.task-panel', {
        tabPanel: '.tab-panel',
        tabPanelBtn: '.tab-panel .btn',
        tabContent: '.tab-content',
        todoTaskList: '.todo-task-list',
        doneTaskList: '.done-task-list'
    });

    var activeTask = new PomodoroApp.ActiveTaskPanel('.active-task');

});
