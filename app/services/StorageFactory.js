(function ($, App) {

    /**
     * Exposing StorageFactory
     * @type {StorageFactory}
     */
    App.StorageFactory = StorageFactory();


    /**
     * @returns {{Object}}
     * @constructor
     */
    function StorageFactory () {
        var self = {};
        self.createStorage = createStorage;

        return self;

        function createStorage(storageName) {
            return new App.LocalStorage(storageName);
        }
    }


}(jQuery, PomodoroApp));