(function ($, App) {
    'use strict';

    /**
     * Component default options
     * @type {{Object}}
     */
    var componentDefaultOptions = {};

    /**
     * Create main class Component
     */
    var Component = Class.extend({
        constructor: function Component(element, options) {
            this.element = $(element);
            this.options =  this.mergeDefaultOptions(options);
            this.__eventEmitter = new App.EventEmitter();
            addEventListeners.call(this);
        },

        mergeDefaultOptions: function(options) {
            return $.extend({}, componentDefaultOptions, options);
        },

        on: function () {
            this.__eventEmitter.on.apply(this.__eventEmitter, arguments);
        },

        emit: function(){
            this.__eventEmitter.emit.apply(this.__eventEmitter, arguments)
        }
    });

    function addEventListeners(){
        // console.info('Component eventListeners still works');
    }

    App.Component = Component;

}(jQuery, PomodoroApp));
