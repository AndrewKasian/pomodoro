(function (global) {
  'use strict';

  global.Class = Class;

  function Class() {}
  Class.extend = function (def) {
    if (!(def.constructor instanceof Function)) {
      throw new Error('Error extending [' + this.name + '] class: constructor must be defined');
    }

    var Parent = this,
    proto = Object.create(this.prototype),
    classDef = decorateWithSuperConstructor(def.constructor);

    extendProto(proto, def);

    classDef.extend = this.extend;
    classDef.prototype = proto;
    classDef.prototype.constructor = def.constructor;

    return classDef;

    function decorateWithSuperConstructor(constructor) {
      return function () {
        if (arguments[0] === Class) {
          return;
        }

        this.super = new Parent(Class);
        constructor.apply(this, arguments);
      };
    }

    function extendProto(proto, def) {
      for (var name in def) {
        if (name === 'constructor') {
          continue;
        }
        proto[name] = def[name];
      }
    }
  };

}(this));