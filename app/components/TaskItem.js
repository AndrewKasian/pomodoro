(function ($, App) {

    /**
     * TaskItem default options
     * @type {{itemClass: string, taskNameClass: string}}
     */
    var itemDefaultOptions = {
        itemClass: 'task-item',
        taskNameClass: 'taskName'
    };

    /**
     * Class TaskItem
     * @type {Object}
     */
    var TaskItem = App.ComponentObservable.extend({
       constructor: function TaskItem(options) {
           this.options = App.ComponentObservable.prototype.mergeDefaultOptions.call(this, options);
           this.element = $('<li class=' + this.options.itemClass + '/>');
            App.ComponentObservable.prototype.constructor.call(this, this.element, this.options);
           this.taskService = PomodoroApp.TaskService.getInstance();
           this.__renderTaskItem();
       },
        mergeDefaultOptions: function (options) {
            return $.extend({},itemDefaultOptions, options);
        },
        __renderTaskItem: function () {
            var taskName = this.options.itemParams.taskName;
            this.element.append($('<span class="'+ this.options.taskNameClass +'">'+ taskName + '</span>'));
        },
        getElement: function () {
            return this.element;
        }

    });

    App.TaskItem = TaskItem;

}(jQuery, PomodoroApp));