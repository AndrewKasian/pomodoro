(function ($, App){

    /**
     * TabPanel default options
     * @type {{tabActiveClass: string, tabContentActiveClass: string}}
     */
    var tabPanelDefaultOptions = {
        tabActiveClass: 'btn-active',
        tabContentActiveClass: 'show'
    };

    /**
     * Class TabPanel
     * @type {Object}
     */
    var TabPanel = App.Component.extend({
        constructor: function (element, options) {
            this.options = App.Component.prototype.mergeDefaultOptions.call(this, options);
            this.super.constructor.call(this, element, this.options);
            this.panelBtn = new App.Button(this.options.tabButtons);
            this.tabContent = $(this.options.tabContent);
            addEventListeners.call(this);
        },
        mergeDefaultOptions: function(options) {
            return $.extend({}, tabPanelDefaultOptions, options);
        },

        __setActiveTab: function(btn){
            $(btn).addClass(this.options.tabActiveClass).siblings().removeClass(this.options.tabActiveClass);
        },
        __setActiveTabContent: function(tabName) {
            this.tabContent.removeClass(this.options.tabContentActiveClass);
            this.tabContent.filter('[data-content=' + tabName + ']').addClass(this.options.tabContentActiveClass);
        }
    });

    function addEventListeners(){
        var that = this;
        this.panelBtn.on('button:click', function(btn){
            var tabName = $(btn).data('tab');
            that.__setActiveTab(btn);
            that.__setActiveTabContent(tabName);
        });
    }

    App.TabPanel = TabPanel;

}(jQuery, PomodoroApp));