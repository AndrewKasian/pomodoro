(function ($, App) {

    /**
     * Class Button
     * @type {Object}
     */
    var Button = App.Component.extend({
        constructor: function (element, options) {
            App.ComponentObservable.prototype.constructor.call(this, element, options);
            addEventListeners.call(this);
        }
    });

    function addEventListeners() {
        var that = this;
        this.element.on('click', function(event){
            that.emit('button:click', $(event.target),  that.options);
        });
    }

    App.Button = Button;

}(jQuery, PomodoroApp));