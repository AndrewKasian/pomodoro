(function ($, App) {

    /**
     * Class - component Timer
     * @type {Object}
     */
    var Timer = App.Component.extend({
        constructor: function Timer(element, options) {
            App.Component.prototype.constructor.call(this, element, options);
            this.btnPomodoro = new App.Button(this.options.btnPomodoro);
            this.btnShortBreak = new App.Button(this.options.btnShortBreak);
            this.btnLongBreak = new App.Button(this.options.btnLongBreak);
            this.btnTimerStart = new App.Button(this.options.btnTimerStart);
            this.btnTimerPause = new App.Button(this.options.btnTimerPause);
            this.clock = new App.Clock(this.options.clock);
            this.taskService = PomodoroApp.TaskService.getInstance();
            this.currentTime = 0;
            this.activeTask = '';
            this.chosenTask = '';
            this.chosenPomodoroTimer = null;
            addEventListeners.call(this);
            setDefaultTime.call(this);
        },

        __startTimer: function () {
            this.clock.start();
        },

        __pauseTimer: function () {
            this.clock.pause();
        },

        __setTime : function (min) {
            this.clock.setTime(min);
        }
    });

    function setDefaultTime() {
        var pomodoroTime = this.btnPomodoro.element.data('time');
        this.__setTime(pomodoroTime);
        this.currentTime = pomodoroTime;
        this.chosenPomodoroTimer = true;
    }

    function addEventListeners() {
        var self = this;

        this.btnPomodoro.on('button:click', function (btn) {
            self.currentTime = btn.data('time');
            self.chosenPomodoroTimer = true;
            self.__setTime(btn.data('time'));
            self.__pauseTimer();
        });
        this.btnShortBreak.on('button:click', function (btn) {
            self.currentTime = btn.data('time');
            self.chosenPomodoroTimer = false;
            self.__setTime(btn.data('time'));
            self.__pauseTimer();
        });

        this.btnLongBreak.on('button:click', function (btn) {
            self.currentTime = btn.data('time');
            self.chosenPomodoroTimer = false;
            self.__setTime(btn.data('time'));
            self.__pauseTimer();
        });
        this.btnTimerStart.on('button:click', function () {
            checkActiveTaskAndStart.call(self);
            self.activeTask = self.chosenTask;
        });
        this.btnTimerPause.on('button:click', function () {
            self.__pauseTimer();
        });
        this.taskService.on('item:choseActiveTask', function (newTask) {
            self.chosenTask = newTask;
        });
    }

    function checkActiveTaskAndStart() {
        if(this.chosenPomodoroTimer){
            if(this.chosenTask){
                if(this.chosenTask !== this.activeTask){
                    this.__setTime(this.currentTime);
                }
                this.__startTimer();
                this.taskService.emit('timer:start');
            }else{
                alert('There is no chosen Task')
            }
        }else{
            this.__startTimer();
        }
    }

    App.Timer = Timer;

}(jQuery, PomodoroApp));